@echo off
setlocal enabledelayedexpansion
set /p api=<..\tools\settings\api.txt
set /p usrc=<..\tools\settings\compression_settings.txt
set baksmali="..\tools\build\baksmali.jar"
set smali="..\tools\build\smali.jar"
set zip="..\tools\Apps\7za.exe"
if (%1)==() goto end
if not exist ..\odex\system\app\%~n1.odex goto notodex
if not exist ..\odex\out mkdir ..\odex\out>>..\tools\logs\log.log 2>&1
echo Deodexing %~n1.apk...
java -jar %baksmali% -a %api% -x ..\odex\system\app\%~n1.odex -d ..\framework -o ..\done\deodexed\system\app\%~n1>>..\tools\logs\log.log 2>&1
if errorlevel 1 goto error
java -jar %smali% -a %api% ..\done\deodexed\system\app\%~n1 -o ..\odex\out\classes.dex>>..\tools\logs\log.log 2>&1
if errorlevel 1 goto error
%zip% u -tzip ..\odex\system\app\%~n1.apk ..\odex\out\classes.dex -mx%usrc%>>..\tools\logs\log.log 2>&1
move ..\odex\system\app\%~n1.apk ..\done\deodexed\system\app\%~n1.apk>>..\tools\logs\log.log 2>&1
if exist ..\done\deodexed\system\app\%~n1 rd /s /q ..\done\deodexed\system\app\%~n1>>..\tools\logs\log.log 2>&1
if exist ..\odex\system\app\%~n1.odex del /q ..\odex\system\app\%~n1.odex>>..\tools\logs\log.log 2>&1
if exist ..\odex\out rd /s /q ..\odex\out>>..\tools\logs\log.log 2>&1
goto end

:notodex
move ..\odex\system\app\%~n1.apk ..\done\deodexed\system\app\%~n1.apk>>..\tools\logs\log.log 2>&1
if exist ..\done\deodexed\system\app\%~n1 rd /s /q ..\odex\system\app\%~n1>>..\tools\logs\log.log 2>&1
if exist ..\odex\system\app\%~n1.odex del /q ..\odex\system\app\%~n1.odex>>..\tools\logs\log.log 2>&1
if exist ..\odex\out rd /s /q ..\odex\out>>..\tools\logs\log.log 2>&1
goto end

:error
echo.
..\tools\apps\chgcolor %themeERR%
echo An Error Occured with %~n1.apk
..\tools\apps\chgcolor %themeBG%
echo An Error Occured with %~n1.apk>>..\odex\errorfile.txt
set /A errors+=1
echo %errors% > ..\odex\error.txt
echo.
rd /s /q ..\odex\system\app\%~n1>>..\tools\logs\log.log 2>&1
:end